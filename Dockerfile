FROM debian:buster-slim

RUN apt-get update && \
    apt-get install -y wget && \
    apt-get install -y curl && \
    curl --location --output /usr/local/bin/release-cli "https://release-cli-downloads.s3.amazonaws.com/latest/release-cli-linux-amd64" && \
    chmod +x /usr/local/bin/release-cli

CMD ["/bin/sh"]